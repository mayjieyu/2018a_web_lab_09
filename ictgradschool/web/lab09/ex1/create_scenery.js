$(document).ready(function () {
    $("#radio-background1").click(function () {
        $("#background").attr("src","../images/background1.jpg");
    });

    $("#radio-background2").click(function () {
        $("#background").attr("src","../images/background2.jpg");
    });

    $("#radio-background3").click(function () {
        $("#background").attr("src","../images/background3.jpg");
    });

    $("#radio-background4").click(function () {
        $("#background").attr("src","../images/background4.jpg");
    });

    $("#radio-background5").click(function () {
        $("#background").attr("src","../images/background5.jpg");
    });

    $("#radio-background6").click(function () {
        $("#background").attr("src","../images/background6.gif");
    });



    $(".dolphin").hide();

    $("#dolphin1").show();

    $("#check-dolphin1").click(function () {
        $("#dolphin1").toggle();
    });

    $("#check-dolphin8").click(function () {
        $("#dolphin2").toggle();
    });

    $("#check-dolphin5").click(function () {
        $("#dolphin3").toggle();
    });

    $("#check-dolphin3").click(function () {
        $("#dolphin4").toggle();
    });

    $("#check-dolphin6").click(function () {
        $("#dolphin5").toggle();
    });

    $("#check-dolphin4").click(function () {
        $("#dolphin6").toggle();
    });

    $("#check-dolphin2").click(function () {
        $("#dolphin7").toggle();
    });

    $("#check-dolphin7").click(function () {
        $("#dolphin8").toggle();
    });


    $("#vertical-control").change(function () {
        var verticalHeight = $(this).val();
        //alert($(this).val());
        console.log(verticalHeight);
        $(".dolphin").each(function () {

            $(this).css("transform","translateY("+ -verticalHeight + "px)");
        });
    });

    $("#horizontal-control").change(function () {
        var horiWidth = $(this).val();
        $(".dolphin").each(function () {
            $(this).css("transform","translateX("+ horiWidth + "px)");
        });
    });

    $("#size-control").change(function () {
        var oriSize = $(this).val();
        oriSize = 1 + oriSize/100;
        $(".dolphin").each(function () {
            $(this).css("transform","scale("+oriSize + ")");
        });
    });


});













